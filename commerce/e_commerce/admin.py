from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from .models import SortCategories, Item, ColorOfItem, SizeOfItem, Order, Basket

admin.site.register(
    SortCategories, DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',

    ),
    list_display_links=(
        'indented_title',
    ),
)

# admin.site.unregister(SortCategories)
admin.site.register([Item, ColorOfItem, SizeOfItem, Order, Basket])
