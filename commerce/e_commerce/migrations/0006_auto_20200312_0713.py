# Generated by Django 3.0.3 on 2020-03-12 07:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('e_commerce', '0005_auto_20200311_0352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='delivery',
            field=models.IntegerField(choices=[(3, 'In a week'), (5, 'When my grandson is born'), (1, 'In 1 day'), (4, 'In a month'), (2, 'In 3 days')], default=3),
        ),
    ]
