# Online Store API

A RESTfull API application for the online store, wrap
docker application and configure automatic deployment to any free or
shareware server. JSON and be able to
handle all possible errors
(log collection is welcome).
The application must be REST-full. All response from the application in
JSON format, including errors. No interface except admin panel for
users do not need to be developed. The functional part of the project
covered by UNIT tests. Endpoints for the API are built at the discretion of the developer. TO
The project should have documentation.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Venv
2. Django
3. Django-restframework


#### Before run this project....
You need to run requirements file in order to install all packages for project.
Command will be: `pip install -r requirements.txt`

#### Docker
This project is also contains some docker files.  You can also run this project under docker.  In order to achive this first install Docker in your system and Docker compose, too.  For more information about Docker, please visit to this link
<a href="https://docs.docker.com/compose/django/" target="_blank">Docker installation</a>